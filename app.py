import flask
from werkzeug.exceptions import InternalServerError,NotFound,MethodNotAllowed
from utils.CException import *
from flask import jsonify
import json
import os
from flask_cors import *
import datetime
from routers.api.router import Blue_print_constructer_api
from logging.config import dictConfig
from utils.configloader import load_confd

if not os.path.exists('log'):
    os.mkdir('log')

dictConfig({
        "version": 1,
        "disable_existing_loggers": False,  # 不覆盖默认配置
        "formatters": {  # 日志输出样式
            "default": {
                "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            }
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",  # 控制台输出
                "level": "DEBUG",
                "formatter": "default",
            },
            "log_file": {
                "class": "logging.handlers.RotatingFileHandler",
                "level": "DEBUG",
                "formatter": "default",   # 日志输出样式对应formatters
                "filename": "./log/flask.log",  # 指定log文件目录
                "maxBytes": 20*1024*1024,   # 文件最大20M
                "backupCount": 10,          # 最多10个文件
                "encoding": "utf8",         # 文件编码
            },

        },
        "root": {
            "level": "DEBUG",  # # handler中的level会覆盖掉这里的level
            "handlers": ["console", "log_file"],
        },
    }
)

assert os.path.exists("cases.json")
assert os.path.exists("changeable_useral.json")
assert os.path.exists("conf.json")

# config Loader
conf:dict = {
    "api_version":"",
    "sitename":"",
    "commands":{}
}
try:
    with open('conf.json','r',encoding='utf-8') as fp:
        conf:dict = json.load(fp)
    
    conf.setdefault('commands',load_confd())
except Exception as e:
    raise Exception(str(e))

app = flask.Flask(conf["sitename"])
CORS(app, resources={r'/*': {'origins': '*'}})


app.config["JSON_AS_ASCII"] = False

# try:
    # app.config['conf'] = conf
    # app.config['command_conf'] = command_conf

    # app.config['conf']['api_version'] = "v"+conf["api_version"]
    # app.config['conf']['debugmode'] = eval(app.config['conf']['debugmode'])
    # app.config['conf']['ifauth'] = eval(app.config['conf']['ifauth'])

    # api_version = app.config['conf']['api_version']
    # debug_mode = app.config['conf']["debugmode"]
# except Exception as e:
    # raise Exception(f"Loading config Failed due to: {str(e)}")
base_path = f'/api/v{conf.get("api_version",1)}'

app.register_blueprint(Blue_print_constructer_api(conf,base_path))

@app.route("/")
def Hello():
    return "Hello World! It's {} in UTC+8:00 BeijingTime".format(datetime.datetime.now())

@app.errorhandler(NotFound)
def handle_404(e):
    return jsonify({
        "code": 404,
        "errmsg":"You seems come to the wrong way",
        "body":{
            # "errinfo":str(e),
            "advice":base_path,
        }
    })

@app.errorhandler(InternalServerError)
def handle_500(e:InternalServerError):
    res = {
        "code":e.code,
        "errmsg":"Ops! It seems somthing wrong with the server, smash it to fix it up",
        "body":{}
    }
    # if debug_mode:
    #     # res["body"]["infile"] = str(e.__traceback__.tb_frame.f_globals["__file__"])
    #     # res["body"]["lineno"] = str(e.__traceback__.tb_lineno)
    #     res["body"]["exception"] = str(e)

    return jsonify(res)

@app.errorhandler(MethodNotAllowed)
def handle_405(e:MethodNotAllowed):
    return jsonify({
        "code":e.code,
        "errmsg":"What about change the method and request again"
    })

def handle_custom(e:NodeException):
    app.logger.warning(str(e.get_body()))
    return jsonify(e.get_body())

app.register_error_handler(ProcessRuntimeError,handle_custom)
app.register_error_handler(ProcessIDError,handle_custom)
app.register_error_handler(ProcessNameError,handle_custom)
app.register_error_handler(PramasError,handle_custom)
app.register_error_handler(SpinterfaceError,handle_custom)
app.register_error_handler(ConnectionRefusedError,handle_custom)
app.register_error_handler(ConfProcessError,handle_custom)
app.register_error_handler(UseralProcessError,handle_custom)

if __name__ == '__main__':
    app.run(port=12207, host='0.0.0.0')
