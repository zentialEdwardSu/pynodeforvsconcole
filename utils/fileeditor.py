import os
import os.path as osp
import shutil

def backup(filepath: str) -> None:
    """
    Backup a file to the same directory with a .back extension.

    Args:
        filepath: The path of the file to be backed up.
    """
    if not os.path.exists(filepath):
        raise FileNotFoundError(f"No such file or directory: '{filepath}'")

    dir_path, file_name = os.path.split(filepath)
    backup_path = os.path.join(dir_path, f"{file_name}.back")
    shutil.copy2(filepath, backup_path)

def restore_backup(backup_path: str) -> None:
    """
    Restore a backup file to its original file.

    Args:
        backup_path: The path of the backup file.
    """
    if not os.path.exists(backup_path):
        raise FileNotFoundError(f"No such file or directory: '{backup_path}'")

    # 目录和文件名
    backup_dir, backup_file = os.path.split(backup_path)
    if not backup_file.endswith('.back'):
        raise ValueError(f"Invalid backup file: '{backup_path}'. Must end with '.back'")

    # 获取原始文件路径
    file_name = backup_file[:-5]  # 去掉 .back 后缀
    file_path = os.path.join(backup_dir, file_name)

    shutil.copy2(backup_path, file_path)

def get_al(path: str) -> str:
    """
    Get the content of a Python file.

    Args:
        path: The path of the Python file.

    Returns:
        The content of the Python file as a string.
    """
    if not os.path.exists(path):
        raise FileNotFoundError(f"No such file or directory: '{path}'")

    if not path.endswith('.py'):
        raise ValueError(f"Invalid file format: '{path}'. Must be a .py file.")

    with open(path, 'r') as f:
        content = f.read()

    return content


def set_al(path: str, code: str) -> None:
    """
    Overwrite the content of a Python file with the given code string, after backing up the original file.

    Args:
        path: The path of the Python file.
        code: The code string to be written to the Python file.
    """
    if not os.path.exists(path):
        raise FileNotFoundError(f"No such file or directory: '{path}'")

    if not path.endswith('.py'):
        raise ValueError(f"Invalid file format: '{path}'. Must be a .py file.")

    backup(path)

    with open(path, 'w') as f:
        f.write(code)
