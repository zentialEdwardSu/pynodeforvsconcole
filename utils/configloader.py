import os
import json

def load_confd() -> dict:
    '''read conf from conf.d'''
    res = {}
    other_json = os.listdir('conf.d')
    if "default.json" in other_json:
        other_json.remove("default.json") # 保证default.json只在最开始会被加载一次
    read_list = ['default.json'] + os.listdir('conf.d') + ['netconf.json'] # 保证netconf的优先级最高
    for eachjson in read_list:
        if not os.path.exists(os.path.join('conf.d', eachjson)) or not eachjson.endswith("json") or os.path.getsize(os.path.join('conf.d', eachjson)) ==  0:
            read_list.remove(eachjson)

    for filename in read_list:
        if filename.endswith(".json"):
            filepath = os.path.join('conf.d', filename)
            
            with open(filepath, "r",encoding="utf-8") as f:
                json_data = json.load(f)

                res.update(json_data)

    return res
                
