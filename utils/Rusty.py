class Result(object):
    '''
    # Result
    A Rusty Result struct to hold return value
    Notice: == can't be use between Result<T>
                    where T: !__eq__
    Notice: Result class are supposed to be genaerate from its static method Ok( ) and Err( ) instead of it's constructor method

    ## BasicUsage:
    ```python
    Ok(1)

    Err(Exception("Some thing unexcepted"))

    Err("errmsg")
    ```

    ## With unwrap()
    ```python
    Ok(1).unwrap() => 1
    # when self._err is a instance of Exception, unwrap will raise it
    Err(Exception("Some thing unexcepted")).unwrap() => an raised Exception with msg "Some thing unexcepted"
    # when self._err is not, unwrap() will just return it
    Err("errmsg").unwrap() => "errmsg"
    ```
    '''
    # __slots__ = ('_val','_err')
    _val = None
    _err = None

    def __init__(self,Ok=None,Err:Exception = None) -> None:
        self._val = Ok
        self._err = Err

    @staticmethod
    def Ok(_val):
        return Result(Ok=_val)
    
    @staticmethod
    def Err(_err):
        return Result(Err=_err)
    
    def unwrap(self):
        if self._val:
            return self._val
        elif self._err != None and isinstance(self._err,Exception):
            raise self._err
        elif self._err != None:
            return self._err
        else:
            raise Exception("Empty result")
    
    def is_Ok(self):
        if self._val:
            return True
        else:
            return False
        
    def __eq__(self, __o) -> bool:
        return self._val == __o._val or self._err == __o._err

# Alias for Result
Err = Result.Err
Ok = Result.Ok