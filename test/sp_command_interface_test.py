import unittest
from utils.sp_command_interface import sp_command_interface
from utils.ProcessHandle import ProcessHandle

class TestSp_command_interface(unittest.TestCase):
    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)

        self.sp_h = ProcessHandle("test\sp_command",isshell=True)
        self.sp_h.start()
        

    def test_it_should_return_start_exercise_without_store(self):
        h = sp_command_interface(self.sp_h).start_exercise_nostore()

        raise Exception(h)

