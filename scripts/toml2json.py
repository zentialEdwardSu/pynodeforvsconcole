import os.path as osp
import os
import toml
import json

if __name__ == '__main__':
    path = './'
    commands_path = f'{path}commands.toml'
    conf_path = f'{path}conf.toml'
    final = {}
    if osp.exists(commands_path) and osp.exists(conf_path):
        raw = toml.load(commands_path)
        for k_name in raw.get('commands',{}).keys():
            print(f'[INFO] resolving module: {k_name}\n')
            final[k_name] = {}
            for part_name in raw.keys():
                part_value = raw.get(part_name,{}).get(k_name,None)
                if part_value:
                    final[k_name][part_name] = part_value if part_name != 'commands' else {'isshell':False,'cmd':part_value}
        # raw_conf = toml.load(conf_path)
        # final.update(raw_conf)
        # raw_conf['commands'] = final
        print('toml sytanx check pass\n')
        print('converting  commands to json\n')
        # print(final)
    
        if not osp.exists("conf.d"):
            os.makedirs("conf.d")
        with open(f'{osp.dirname(path)}//conf.d/default.json','w',encoding='utf-8') as f:
            json.dump(final,f,sort_keys=True,indent=4,separators=(',',':'))

        print("[INFO] converting base conf\n")
        print(toml.load(conf_path))
        with open("conf.json",'w',encoding='utf-8') as f:
            json.dump(toml.load(conf_path),f,sort_keys=True,indent=4,separators=(',',':'))

        print('Done')