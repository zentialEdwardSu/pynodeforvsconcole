from utils.ProcessHandle import ProcessHandle
from utils.Rusty import Result,Err,Ok
from utils.CException import SpinterfaceError

class sp_command_interface(object):
    '''
    ## Wrapper Class 
    Wrapper Class for sp_command

    Notice: Result here will be pass to the topper func to handle

    #### version 0.2-22feb26

    Tested: not passeds
    '''
    def __init__(self,sp_handler:ProcessHandle) -> None:
        self.sp_handler:ProcessHandle = sp_handler
        self.started_lock = False
        self.select_lock = False
        self._mode:int = 0

    def _launcher(self,_c:str) -> Result:
        try:
            self._poll_and_restart()
            if not self.__in_start() and not self.__in_select():
                res = self.sp_handler.write_pipe(self._command_with_y(_c)).unwrap()
                self._set_started_lock_T()
                # if res.is_Ok():
                return Ok(res)
            else:
                return Err(SpinterfaceError('In replay select stage or exercise already running'))
        except Exception as e:
            return  Err(SpinterfaceError(str(e)))
        
    def start_exercise_nostore_antiterror(self) -> Result:
        '''
        在 反恐 不储存 的情况下开始演习
        '''
        if self._mode == 2:
            self._restart()
        self._set_mode(1)
        return self._launcher('1\n0\n')

    def start_exercise_store_antiterror(self) -> Result:
        '''
        在 反恐 储存 的情况下开始演习
        '''
        if self._mode == 2:
            self._restart()
        self._set_mode(1)
        return self._launcher('1\n1\n')

    def start_exercise_nostore_field(self) -> Result:
        '''
        在 田园 不储存 的情况下开始演习
        '''
        if self._mode == 1:
            self._restart()
        self._set_mode(2)
        return self._launcher('2\n0\n')

    def start_exercise_store_field(self) -> Result:
        '''
        在 田园 储存 的情况下开始演习
        '''
        if self._mode == 1:
            self._restart()
        self._set_mode(2)
        return self._launcher('2\n1\n')

    def stop_exercises(self) -> Result:
        '''
        停止当前的演习
        '''
        # self._poll_and_restart()
        if self.__in_start() and not self.__in_select():
            self.sp_handler.write_pipe('y\n').unwrap()
            self._set_started_lock_F()
            return Ok('OK')
        else:
            return Err(SpinterfaceError("Stop before run"))

    def replay_exercise(self,_i) -> Result:
        '''
        '''
        try:
            if self.__in_select() and not self.__in_start():
                self._set_select_lock_F()
                self.sp_handler.call_continue()
                return self.sp_handler.write_pipe(f'{_i}\n')
            else:
                return Err(SpinterfaceError("Not available in select or running"))
        except Exception as e:
            return Err(SpinterfaceError(str(e)))

    # def list_store(self) -> Result:
    #     '''
    #     `Depresed!!`
    #     '''
    #     if not self.sellect_lock and self.started_lock:
    #         try:
    #             self.sp_handler.write_pipe('3\n0\n').unwrap()
    #             return self.sp_handler.read_pipe(1024*4)
    #         except:
    #             return Err(Exception("Not available in select or running"))

    def list_store(self) -> Result:
        '''
        `NotStableFeature`
        '''
        self._poll_and_restart()
        try:
            if not self.__in_start() and not self.__in_select():
                self.sp_handler.write_pipe(f'3\n')
                self._set_select_lock_T()
                res = self.sp_handler.read_pipe(8*1024).unwrap()
                # try to use SIGSTOP to pause program
                self.sp_handler.call_stop()
                return Ok(res)
            else:
                return Err(SpinterfaceError("Not available in select or running"))
        except Exception as e:
            return  Err(SpinterfaceError(str(e)))

    @staticmethod
    def _command_with_y(c:str) -> str:
        return c+'y\n'
    
    def _set_started_lock_T(self):
        self.started_lock = True
    def _set_started_lock_F(self):
        self.started_lock = False
    
    def _set_select_lock_T(self):
        self.select_lock = True

    def _set_select_lock_F(self):
        self.select_lock = False

    def _poll_and_restart(self):
        if self.sp_handler.get_status() != 'None':
            self._restart()

    def _restart(self):
        self._mode = 0
        self.sp_handler.stop()
        self.sp_handler.start()  

    def _set_mode(self, mode:int):
        self._mode = mode  
    
    def __in_select(self):
        return self.select_lock
    
    def __in_start(self):
        return self.started_lock

    