import uuid
import sys
from utils.res import json_200
from flask import Blueprint,request,jsonify
from utils.ProcessHandle import ProcessHandle
from utils.Rusty import Err, Ok, Result
from utils.safewarp import boolwarp, safewarp
from utils.sp_command_interface import sp_command_interface
from utils.CException import PramasError, ProcessIDError, ProcessNameError,ProcessRuntimeError,SpinterfaceError

def Blue_print_constructer_shit(config):

    commands = config['command_conf']["commands"]
    workdir = config['command_conf']["workdir"]
    excoms = config['command_conf']["excom"]
    envs = config['command_conf']["env"]

    shit = Blueprint('shit', __name__,url_prefix="/shit")
    sp_env = {
        "MONGO_HOME":"/home/linyu/mongodb",
        "PROTOC_HOME":"/home/linyu/include/protobuf",
        "PATH":"$MONGO_HOME/bin:$PROTOC_HOME/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin",
        "LD_LIBRARY_PATH":"/home/linyu/include/protobuf/lib:/home/linyu/engine/mongo-c-driver-1.21.0/lib:/home/linyu/engine/zeromq-4.3.4/lib/lib:/home/linyu/engine/protobuf-3.19.4/LIB/lib:/home/linyu/engine/lib/:$LD_LIBRARY_PATH"
    }
    process = {}
    sp_process:list[sp_command_interface] = []

    @shit.route("/startprocess")
    def shit_srtart_process():
        argdata = request.args.to_dict()
        return start_handler(argdata)
        
    @shit.route("/stopprocess")
    def shit_stop_process():
        argdata = request.args.to_dict()
        return stop_handler(argdata)

    @shit.route("/scriptstatus")
    def scriptstatus():
        res = {
            "code":200,
            "errmsg":"",
            "body":{
                "acceptcommands":[x for x in commands.keys()],
                "current_processes":{k:{"command":process[k]["command"],"status":process[k]["process"].get_status()} for k in process.keys()},
                # "healthy_check":target_check()
            }
        }
        return jsonify(res)
    
    @shit.route("/shutall")
    def shutall():
        al_stoped = []
        for each_id in process.keys():
            al_stoped.append(_stop_process({"process_id":str(each_id)},process).unwrap())

        return jsonify({
            "code": 200,
            "errmsg":"",
            "body":{
                "msg":f"{str(al_stoped)} has been shutdown",
            }
        })
    if sys.platform != "win32":
        
        @shit.route('/sp/new')
        def sp_new_interface():
            p = ProcessHandle("/home/huangxiaojie/command/Sp_command_student",env=sp_env,isshell=True)
            p.start()
            sp_process.append(sp_command_interface(p))

            return jsonify(json_200({"msg":"new interface has been created"}))

        @shit.route("/sp/start")
        def sp_start_exercise():
            # argdata = request.args.to_dict()
            scenes = request.args.get("scenes","No",type=str)
            ifstore = request.args.get("ifstore",-1,type=int)
            assert_sp_(sp_process).unwrap()
            s = sp_process[0]
            jobs = {
                "anti":[s.start_exercise_nostore_antiterror,s.start_exercise_store_antiterror],
                "field":[s.start_exercise_nostore_field,s.start_exercise_store_field],
            }
            if scenes in ['anti','field'] and ifstore in [0,1]:
                jobs[scenes][int(ifstore)]().unwrap()
            else:
                raise SpinterfaceError("{} with store state {} not supported".format(scenes,ifstore))
            
            return jsonify(json_200({"msg":f"{scenes} with ifstore: {ifstore} started"}))

        @shit.route("/sp/stop")
        def sp_stop_exercise():
            assert_sp_(sp_process).unwrap()
            sp_process[0].stop_exercises()
            return jsonify(json_200({"msg":"current process stoped"}))

        @shit.route("/sp/list")
        def sp_list_store():
            assert_sp_(sp_process).unwrap()
            sp_process[0].list_store().unwrap()
            return jsonify({"store":sp_process[0].list_store().unwrap()})
        
        @shit.route("/sp/replay")
        def sp_replay():
            argdata = request.args.to_dict()
            assert_sp_(sp_process).unwrap()
            id = argdata['storeid']
            sp_process[0].replay_exercise(id)
            return jsonify({"msg":"start replay using data {id}"})
        
        @shit.route("/sp/destory")
        def destory():
            assert_sp_(sp_process).unwrap()
            if sp_process[0].__in_start() or sp_process[0].__in_select():
                raise SpinterfaceError("Stop current exercise or finish selecting before exit")
            sp_process[0].sp_handler.stop()
            sp_process.clear()
            return jsonify(json_200({"msg":"sp command interface exit"}))

    def start_handler(data:dict):
        (process_id, process_handle) = _start_process(data,commands,workdir,excoms,envs).unwrap()
        process[str(process_id)] = process_handle
        return jsonify({
                    "code":200,
                    "errmsg":"",
                    "body":{
                        "process_id":process_id
                    }
                })

    def stop_handler(data:dict):
        return jsonify({
            "code":200,
            "errmsg":"",
            "body":{
                "message":f"send SIGTERM to {_stop_process(data,process).unwrap()} "
            }
        })
    
    def _start_process(data:dict,commands:list,workdir:dict,excoms:dict,envs:dict) -> Result:
        '''
        data struct:{"process":<ProcessHandle>,"command":<str>"(startcommand)}
        commands a commands dict load fom toml
        workdir a commands dict load fom toml
        excoms a commands dict load fom toml
        '''

        token = safewarp(data,"token","")
        command = safewarp(data,"process_name","")
        process_id = uuid.uuid4()
        process_isshell = boolwarp(safewarp(data,"process_isshell",""))
        if process_isshell is None or command == "":
            raise PramasError(f"Consider check the parmas {'process_name' if command == '' else 'process_isshell'}")

        if command in commands:
            try:
                process_handle = {"process":ProcessHandle(args=commands[command],\
                                                        cwd=safewarp(workdir,command,None),\
                                                        isshell=process_isshell,\
                                                        excom=safewarp(excoms,command,None),\
                                                        env=safewarp(envs,command,None))}
                process_handle["process"].start()
                process_handle["command"] = command

                return Ok((process_id,process_handle))
            except Exception as e:
                Err(ProcessRuntimeError(str(e)))
        else:
            Err(ProcessNameError("Request command not found in conf.toml"))


    def _stop_process(data:dict,process:dict) -> Result:
        token = safewarp(data,'token',"")
        process_id = safewarp(data,'process_id',"")

        if process_id not in process:
            raise ProcessIDError(f"{process_id} is not vaild id")
        
        try:
            process[process_id]["process"].stop()
            del process[process_id]
            return Ok(process_id)
        except Exception as e:
            Err(ProcessRuntimeError(str(e)))

    def assert_sp_(process:list) -> Result:
        if len(process) == 0:
            return Err(SpinterfaceError("spinterface not created"))
        else:
            return Ok(1)

    return shit