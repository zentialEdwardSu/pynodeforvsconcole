from subprocess import Popen,PIPE
from utils.CException import *
from utils.safewarp import safewarp,boolwarp,easy_wrap
import uuid
from utils.Rusty import Result,Err,Ok
import socket
import sys

# from typing import IO

class ProcessHandleexpection(Exception):
    '''
    '''
    def __init__(self,error:str):
        self.error_msg = error
    def __str__(self):
        return ("ProcessHandleError > {}".format(self.error_msg))

class ProcessHandle(object):
    '''
    This is a class for handling shell or application processes
    ### version 0.7.0-23feb19
    '''
    def __init__(self,args,excom=None,cwd=None,env=None,isshell = False,**otherpopenarg) -> None:
        self.args = args
        self.program = None
        self.shell = isshell
        self.cwd = cwd
        # if cwd != "":
        #     self.cwd = cwd
        # else:
        #     self.cwd = None
        # if excom != "":
        #     self.excom = excom
        # else:
        #     self.excom = None
        self.cwd:str = easy_wrap(cwd,None)
        # if self.cwd:
        #     self.cwd = self.cwd.encode('utf-8')
        #     print(self.cwd)
        self.excom = easy_wrap(excom,None)
        self.env = easy_wrap(env,None)
        self.extra_arg = otherpopenarg
        if isshell :
            # 使用socket连接子进程的 stdin stdout stderr
            if sys.platform != "win32":
                self.s_local, self.s_remote = socket.socketpair(socket.AF_UNIX, socket.SOCK_STREAM)
            else:
                self.s_local,self.s_remote = (PIPE,PIPE)

    def start(self):
        if self.args is not None:
            if not self.shell:
                self.program = Popen(args=self.args,cwd=self.cwd,env=self.env,**self.extra_arg)
            else:
                self.program = Popen(args=self.args,shell=True,stdin=self.s_remote,stdout=self.s_remote,stderr=self.s_remote,cwd=self.cwd,env=self.env,bufsize=0)
                if sys.platform != "win32":
                    # 关闭远程管道
                    self.s_remote.close()
            if self.program.poll() != None:
                self.program = None
                raise ProcessHandleexpection("Can't create process")
        else:
            raise ProcessHandleexpection("No available args")
        
    def stop(self):
        if self.program:
            self.program.terminate()
            # if self.program.poll() is None:
            #     raise ProcessHandleexpection("Can't stop the process")
            # 执行额外指令清除进程
            if self.excom:
                Popen(self.excom,shell=True,start_new_session=True)
            if self.program.poll() is not None:
                self.program = None
        else:
            raise ProcessHandleexpection("You shuould run the process first")

    def get_pipe(self) -> Result:
        '''
        Return:
            Ok((stdin,stdout,stderr))
        '''
        if(self.shell):
            return Ok((self.program.stdin,self.program.stdout,self.program.stderr))
        else:
            raise Err(ProcessHandleexpection("get_pipe only available while self.shell is True"))

    def get_status(self):
        '''
        get current status of handled-process'''
        return str(self.program.poll())
    
    def is_shell_mode(self) -> bool:
        return self.shell
    
    def call_stop(self):
        if sys.platform != 'Win32':
            import signal
            self.program.send_signal(signal.SIGSTOP)
        else:
            return ()
        
    def call_continue(self):
        if sys.platform != 'Win32':
            import signal
            self.program.send_signal(signal.SIGCONT)
        else:
            return ()
    
    def call_stop(self):
        if sys.platform != 'Win32':
            import signal
            self.program.send_signal(signal.SIGSTOP)
        else:
            return ()
        
    def call_continue(self):
        if sys.platform != 'Win32':
            import signal
            self.program.send_signal(signal.SIGCONT)
        else:
            return ()
    
    def write_pipe(self,msg:str) -> Result:
        '''
        Unix feature!!
        Notice: Availiable in shell mode it will return a `Result::Err()` when isn't in shell mode

        msg: a text msg to pass in 

        Return: Result<( ),Err: Exception>
        '''
        if self.is_shell_mode() and self.program:
            try:
                # write msg\n to local socket 
                writen_bit = self.s_local.send((msg+'\n').encode())
                if writen_bit < len(msg) - 1:
                    return Err(ProcessHandleexpection('Didn\'t write all'))
                return Ok(writen_bit)
            except Exception as e:
                return Err(e)
        else:
            return Err(ProcessHandleexpection("Make sure program is running and in shell mode"))


    def read_pipe(self,bufsize = 1024):
        '''
        Unix feature!!

        Notice: Availiable in shell mode it will return a Result::Err() when isn't in shell mode

        - bufsize: size of reader's buffer

        - Return: Result<out_msg: str,Err: Exception>
        '''
        if self.is_shell_mode() and self.program:
            try:
                # read msg from socket
                out_msg = self.s_local.recv(bufsize).decode()
                return Ok(out_msg)
            except Exception as e:
                return Err(e)
        else:
            print(self.shell)
            return Err(ProcessHandleexpection("Make sure program is running and in shell mode"))
            # return Err(Exception("Make sure program is running and in shell mode"))

# def util_tuple_decode(l):
#     res = []
#     for each in l:

