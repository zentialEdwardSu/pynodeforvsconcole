from werkzeug.exceptions import HTTPException
import json

class NodeException(HTTPException):
    code = 400
    msg = "Base exception for pynode"
    hint = ""
    errcode = 1000
    def __init__(self, msg=None,code=None,errcode=None,hint=None) -> None:
        super().__init__(msg,None)
        if code:
            self.code = code
        if msg:
            self.msg = msg
        if errcode:
            self.errcode = errcode
        if hint:
            self.hint = hint

    def get_body(self,environ=None):
        body = {
            "errmsg":f"{self.msg}:{self.hint}",
            "code": self.errcode
        }
        return body
    def get_headers(self, environ=None):
        return [("Content-Type","application/json")]
    
    def __str__(self):
        return json.dumps(self.get_body(),sort_keys=True,indent=4,separators=(',',':'))
class PramasError(NodeException):
    def __init__(self,hint=None) -> None:
        super().__init__("ParamasError", 400, 1201, hint)

class ProcessRuntimeError(NodeException):
    def __init__(self,hint=None) -> None:
        super().__init__("ProcesRuntimeError", 400, 1202, hint)

class ProcessNameError(NodeException):
    def __init__(self,hint=None) -> None:
        super().__init__("ProcesNameError", 400, 12021, hint)

class ProcessIDError(NodeException):
    def __init__(self,hint=None) -> None:
        super().__init__("ProcesIDError", 400, 12022, hint)

class SpinterfaceError(NodeException):
    def __init__(self,hint=None) -> None:
        super().__init__("Spinterface", 400, 1203, hint)

class ConfProcessError(NodeException):
    def __init__(self,hint=None) -> None:
        super().__init__("ConfProcessError", 400, 1204, hint)

class UseralProcessError(NodeException):
    def __init__(self,hint=None) -> None:
        super().__init__("UseralProcessError", 400, 1205, hint)