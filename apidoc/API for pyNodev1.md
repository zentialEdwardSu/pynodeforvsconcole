# API for pyNode

**apiversion: v1.1.5**

> json中的()代表对应的变量类型而非具体变量名称

**使用params的接口目前有`startprocess`,`stopprocess`,，在路径前添加`/shit/`以调用使用params的`GET`接口**
> 如 `api/v1/startprocess` -> `api/v1/shit/startprocess?token=[]&process_name&process_isshell=[]`
> `process_isshell` 可以接收true/True  false/False 四种形式的参数
🎉 由于项目层级重构与时间因素 Restful接口与params接口的数据将不互通， 故提供`/shit/scriptstatus`查询通过params启动的进程


同时请注意请求参数要求仍然按照之前的参数要求

**GET** `服务器地址/api/v1/`

说明： 根api用于获取api相关信息

请求参数：**无**

返回消息体:

```json
{
    "code":200,
    "errmsg":"",
    "body":{
    	"links":{
    		("返回可用的请求地址")
		},
		"acceptcommands":[  (返回可接受的开始进程的命令) ]
	}
}
```

**GET** `服务器地址/api/v1/info`

说明： 根api用于获取api相关信息

请求参数：**无**

返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"api_version":"v1",
		"authneeded":"false"
	}
}
```

**POST** `服务器地址/api/v1/startprocess`

说明： 通过processname指定要启动的进程

请求参数：

|请求参数|类型|是否必要|说明|
|-|-|-|-|
|process_name|string|是|指定启动的进程|
|token|string|否(目前)|鉴权|
|process_isshell|boolean|是|是否以sheel形式启动并启用标准输入输出管道|

> process_name 来自`/api/v1/`返回值的`acceptcommands`字段
> 在使用`12`节点的两条指令时`is_shell`为`True`在使用`15`,`16`节点时务必不能为`True`,否则会导致进程控制无法控制

返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"process_id":创建的进程id
	}
}
```

创建失败
```json
{
	"code":1202,
	"errmsg":"(报错信息)",
}
```

传入的process_name不被支持
```json
{
	"code":12021,
	"errmsg":"Request command not found in commands.toml"
}
```

**PATCH** `服务器地址/api/v1/stopprocess`

说明：通过`startprocess`返回的process_id终止对应的进程

请求参数：
|请求参数|类型|是否必要|说明|
|-|-|-|-|
|process_id|string|是|通过进程号要停止的进程|
|token|string|否(目前)|鉴权|

返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"message":"(进程号) stoped successfully"
	}
}
```
无效的进程号
```json
{
	"code":12022,
	"errmsg":"(进程号) is not vaild id"
}
```
停止时发生错误
```json
{
	"code":1202,
	"errmsg":"(报错信息)"
}
```

**GET** `服务器地址/api/v1//scriptstatus`
说明：检查命令可用性并返回脚本状态
请求参数：无
返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"acceptcommands":[  (返回可接受的开始进程的命令) ],
		"current_process":{ ...
				"(process_id)":{
					"command": ,//使用的命令
					"status":, //当前进程的运行状态
				}
		...},
		"healthy_check":{
			...
			"(command)":{
				"exists": boolean,//是否存在
				"implable":boolean,//能否执行
			}
			...
		}
	}
}
```
> `status` 为 None 为正常运行状态


## shity api 独有的对接sp_command的接口

**GET** `服务器地址/api/v1/shit/sp/new`

说明：启动新的sp_command实例用于控制

返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"msg":"new interface has been created"
	}
}
```

**GET** `服务器地址/api/v1/shit/sp/start`

说明：通过指定的场景和存储模式启动演习

请求参数：Params
|请求参数|类型|是否必要|说明|
|-|-|-|-|
|scenes|string|是|场景名称|
|ifstore|int|是|是否启用存储|

> 'anti'反恐 'field' 农田
> 'ifsore' 0不启动存储 1启动存储


返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"msg":"'scenes' with ifstore:'ifstore'started"
	}
}
```

**GET** `服务器地址/api/v1/shit/sp/stop`

说明：停止当前正在运行的演习

返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"msg":"current process stoped"
	}
}
```

**GET** `服务器地址/api/v1/shit/sp/list`

说明：获得已记录的存储

返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"store":"(代表存贮信息的裸字符串)"
	}
}
```

**GET** `服务器地址/api/v1/shit/sp/replay`

说明：通过指定的场景和存储模式启动演习

请求参数：Params
|请求参数|类型|是否必要|说明|
|-|-|-|-|
|storeid|string|是|场景名称|

返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"msg":"'start replay using data {storeid}"
	}
}
```

**GET** `服务器地址/api/v1/shit/sp/destory`

说明：销毁当前的sp_command_iinterface

返回消息体:
```json
{
	"code":200,
	"errmsg":"",
	"body":{
		"msg":"sp command interface exit"
	}
}
```


## 自定义错误代码
- **1201** 请求参数不符合要求
- **1202** 进程控制出错
	- 12021 未找到`process_name`
	- **12022** 未找到`process_id`
