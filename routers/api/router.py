import uuid
from flask import Blueprint,current_app
from werkzeug.exceptions import NotFound
from utils.CException import *
from utils.ProcessHandle import ProcessHandle
from utils.Rusty import Err,Ok,Result
from utils.configloader import load_confd
from utils.safewarp import safewarp,boolwarp
from utils.res import json_200
from utils.sp_command_interface import sp_command_interface
from flask import jsonify,request,Config
import os
# from flask_cors import *
import sys
from utils import fileeditor

from routers.test.router import Blue_print_constructer_test
# from ..api.router import Blue_print_constructer_shit


def Blue_print_constructer_api(config:dict,route_base:str):
    api_version = config.get("api_version",1)
    # debug_mode = config['conf']["debugmode"]
    # auth_needed = config['conf']["ifauth"]
    commands:dict = config.get('commands',{})
    # commands_friendlyname = commands.keys()
    # uncheck = config['command_conf']["health"]
    # workdir = config['command_conf']["workdir"]
    # excoms = config['command_conf']["excom"]
    # envs = config['command_conf']['env']

    base_path = route_base
    gateway_methods = {
        "startprocess":["POST"],
        "stopprocess":["PATCH","POST"],
        "updatecommands":["PATCH"],
        "startexercise":["POST"],
        "stopexercise":["GET","PATCH"],
        "replay":["POST"],
    }

    sp_env = {
        "MONGO_HOME":"/home/linyu/mongodb",
        "PROTOC_HOME":"/home/linyu/include/protobuf",
        "PATH":"$MONGO_HOME/bin:$PROTOC_HOME/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin",
        "LD_LIBRARY_PATH":"/home/linyu/include/protobuf/lib:/home/linyu/engine/mongo-c-driver-1.21.0/lib:/home/linyu/engine/zeromq-4.3.4/lib/lib:/home/linyu/engine/protobuf-3.19.4/LIB/lib:/home/linyu/engine/lib/:$LD_LIBRARY_PATH"
    }
    process = {}
    sp_process:list[sp_command_interface] = []
    api = Blueprint(f"api{api_version}", __name__,url_prefix=base_path)
    api.register_blueprint(Blue_print_constructer_test())


    @api.route('')
    def root():
        # aprocessname = [x for x in commands.keys()]
        return jsonify({
            "code": 200,
            "errmsg":"",
            "body":{
                "links":{
                    "current_url":base_path,
                    "api_info_url":base_path+"/info",
                    "start_process_url":base_path+"/startprocess",
                    "stop_process_url":base_path+"/stopprocess",
                    # "process_list":base_path+"/processlist",
                    # "jwt_auth_url":base_path+"/auth",
                    "to_set_commands":"POST"+base_path+"/commands",
                    "to_get_commands":"GET"+base_path+"/commands",
                    "script_status":base_path+"/scriptstatus",
                },
                "acceptcommands":list(commands.keys())
            }
        })

    @api.route("/info")
    def get_info():
        return jsonify(json_200({
                "api_version":"v1",
                "authneeded":"false"
            }))

    @api.route("/startprocess",methods=gateway_methods["startprocess"])
    def rest_start_process():
        data:dict = json.loads(request.data.decode('utf-8'))
        return start_handler(data)

    @api.route("/stopprocess",methods=gateway_methods["stopprocess"])
    def rest_stop_process():
        data:dict = json.loads(request.data.decode('utf-8'))
        return stop_handler(data)

    # @api.route("/auth")
    # def auth():
    #     raise NotFound("This is not a valid inerface currently")

    @api.route("/scriptstatus")
    def scriptstatus():
        res = {
            "code":200,
            "errmsg":"",
            "body":{
                "acceptcommands":[x for x in commands.keys()],
                "current_processes":{k:{"command":process[k]["command"],"status":process[k]["process"].get_status()} for k in process.keys()},
                "healthy_check":target_check()
            }
        }
        return jsonify(res)
    
    @api.route("/shutall")
    def shutall():
        try:
            al_stoped = []
            for each_id in process.keys():
                al_stoped.append(_stop_process({"process_id":str(each_id)},process).unwrap())

            return jsonify(json_200({"msg":f"{str(al_stoped)} has been shutdown"}))
        except Exception as e:
            raise ProcessRuntimeError(str(e)) from e
    
    @api.route("/commands")
    def get_commands():
        '''get all commands'''
        conf = load_confd()
        if conf:
            return jsonify(json_200({"data":conf}))
        else:
            return jsonify(json_200({"msg": "not config found"}))

    @api.route("/commands",methods=['POST'])
    def set_commands():
        '''set commands to netconf.json
        Request:
        {
            "action": add / del
            "data":{
                0...
                "command_friedly_name_n": {
                    'commands': {
                        'cmd': 'for showcase',
                        'isshell': true
                    }
                },
                "command_friedly_name_n+1": {},
                "startM300":{
                    "commands":{
                        "cmd":"python desert-M300-10-proxy.py",
                        "isshell":false
                    },
                    "excom":"taskkill /f /im matlab.exe",
                    "workdir":"D:/jingshen/Dy/Dynamic/plane/M300/Dynamics/M300_point_control_0-9"
                }
                ...Inf
            }
        }
        '''
        nonlocal commands
        request_body:dict = json.loads(request.data.decode('utf-8'))
        current_app.logger.debug(f"use {request_body} to call set_commands")
        action_type = request_body.get('action','NaA')
        data = request_body.get('data','NaD')
        netconfig_path = os.path.join('conf.d','netconf.json')
        current_app.logger.debug(f"use {data} to {action_type} netdonf")
        if data == 'NaD' or action_type == 'NaA' or data == None or action_type == None:
            raise ConfProcessError(f"Invaild request data for ediit conf {request_body}")

        if action_type == "add":
            commands.update(do_add(netconfig_path,data))
        elif action_type == "del":
            (commands,removed__key) = do_del(netconfig_path,data,commands)
            # commands.update(af_commands)
            return jsonify(json_200({"msg":f"{removed__key} has been removed"}))
        else:
            raise ConfProcessError("Invaild action type")
        current_app.logger.debug(f"after process conf: {commands}")
        
        return jsonify(json_200({"msg": f"{data} has been added to netconf" }))

    def do_add(netconfig_path:str,data:dict):
        try:
            for action_name,each_action in data.items():
                # print(each_action['commands']['isshell'])
                isshell_option = each_action.get('commands',{}).get("isshell","NaIS")
                print(isshell_option)
                if isshell_option == "NAIS":
                    raise ConfProcessError("Could not find isshell option in commands/isshell")
                each_action["commands"]["isshell"] = boolwarp(isshell_option,"NaB")
                data[action_name] = each_action
            if not os.path.exists(netconfig_path) or os.path.getsize(netconfig_path) == 0:
                with open(netconfig_path,'w',encoding='utf-8') as f:
                    json.dump(data,f,ensure_ascii=False)
            else:
                pre_conf = {}
                with open(netconfig_path,'r',encoding='utf-8') as f:
                    pre_conf:dict = json.load(f)
                    current_app.logger.debug(f"current static conf {pre_conf}")
                with open(netconfig_path,'w',encoding='utf-8') as f:
                    pre_conf.update(data)
                    json.dump(pre_conf,f,ensure_ascii=False)
                return pre_conf
        except (IOError,json.JSONDecodeError) as exc:
            raise ConfProcessError(str(exc))
        
    def do_del(netconfig_path:str,data:dict,commands:dict):
        try:
            if not os.path.exists(netconfig_path) or os.path.getsize(netconfig_path) == 0:
                raise ConfProcessError("No config found in netconfig.json try to add some before you del some")
            else:
                remove_key = []
                pre_conf = {}
                with open(netconfig_path,'r',encoding='utf-8') as f:
                    pre_conf:dict = json.load(f)
                    current_app.logger.debug(f"current static conf {pre_conf}")
                for each_rm_key in data.keys():
                    if each_rm_key in pre_conf:
                        remove_key.append(each_rm_key)
                        del pre_conf[each_rm_key]
                        del commands[each_rm_key]
                with open(netconfig_path,'w',encoding='utf-8') as f:
                    json.dump(pre_conf,f,ensure_ascii=False)
                return (commands,remove_key)
        except (IOError,json.JSONDecodeError) as exc:
            raise ConfProcessError(str(exc))

    @api.route("/useral")
    def get_useral():
        '''
        'body':{
            'userals':[…… 返回所有可以支持更改的方法 目前标记的规则是读取changeable_useral.json
                {
                    'code': 字符串形式的代码
                    'run_process_name': 用于启动用户算法的process_name
                },
            ……] 
        } //错误时body为空
        '''
        try:
            userals = []
            changeable_useral = {}
            with open("changeable_useral.json",'r',encoding='utf-8') as fp:
                changeable_useral:dict = json.load(fp)

            for _,each_useral in changeable_useral.items():
                code = fileeditor.get_al(each_useral.get("file"))
                userals.append({
                    'code':code,
                    'run_process_name':each_useral.get("run_process_name")
                })
            return jsonify(json_200({'userals':userals}))
        except (FileNotFoundError,json.JSONDecodeError) as e:
            raise UseralProcessError(str(e))

    @api.route("/useral",methods=["POST","PATCH"])
    def set_useral():
        '''
        {
            'run_process_name': 更改的用户算法的process_name 用于确定保存位置
            'code': 用户要更改的代码 全部 字符串形式
        }'''
        try:
            data:dict = json.loads(request.data.decode('utf-8'))
            current_app.logger.debug(f"use {data} to set useral")
            process_name = data.get('run_process_name','NaName')
            code = data.get("code","NaC")
            if process_name == "NaName" or process_name not in commands.keys():
                raise UseralProcessError(f"check run_process_name params current value:{process_name} if it is missing or invaild")
            if code == "NaC":
                raise UseralProcessError(f"check code params to see if it exsist")
            
            changeable_useral = {}
            with open("changeable_useral.json",'r',encoding='utf-8') as fp:
                changeable_useral:dict = json.load(fp)

            # 获取要更改的路径
            target_path = "NaP"
            for _,each_changeable_section in changeable_useral.items():
                if each_changeable_section['run_process_name'] != process_name:
                    continue

                target_path = each_changeable_section['file']

            if target_path == "NaP":
                raise UseralProcessError(f"{process_name} not in changeable_useral.json")
            
            fileeditor.backup(target_path)
            fileeditor.set_al(target_path,code)
            return jsonify(json_200({}))
        except (FileNotFoundError,ValueError) as e:
            raise UseralProcessError(f"failed to set user al because {str(e)}")
         
    # @api.route("/restore_useral",methods=["POST"])
    # def set_useral():
    #     '''
    #     {
    #         'process_name': 更改的用户算法的process_name 用于确定文件
    #     }'''
    #     try:
    #         data:dict = json.loads(request.data.decode('utf-8'))
    #         process_name = data.get('process_name','NaName')
    #         if process_name == "NaName" or process_name not in commands.keys():
    #             raise UseralProcessError('check process_name params if it is missing or invaild')
            




    @api.route("/searchcom",methods=["POST"])
    def search_commands():
        '''
        {
            'case_name': 要搜索的案例名 如 field2 desert3
        },
        'body'{
            'process_names':[
                    '案例下含有的所有process_name'
                ]
        }
        '''
        data:dict = json.loads(request.data.decode('utf-8'))
        target_name = data.get('case_name','NaName')
        if target_name == 'NaName':
            raise ConfProcessError(f"check case_name params in post data")
        current_app.logger.info(f'searching process_name under cases {target_name}')
        res = []
        for each_action_name in commands.keys():
            spilted = each_action_name.split('_')
            if len(spilted) == 4: # 只在符合规则的name中查找
                case_name = spilted[1]
                if case_name == target_name:
                    res.append(each_action_name)
            else: # 不符合规则的跳过
                continue
        return jsonify(json_200({'process_names':res}))


    @api.route("/cases")
    def get_cases():
        '''
        'body':{
            “name of case": {
                    ”friendly_name":"friendly name of cases",
                    "brief":brief of cases"
            },
            "desert1":{
                    ”friendly_name":"沙漠反恐案例一",
                    "brief":使用了3旋翼/1固定翼+互动"
                
            }
        } // 错误时body为空
        '''
        cases = {}
        try:
            with open("cases.json",'r',encoding='utf-8') as case_fp:
                cases = json.load(case_fp)
                current_app.logger.info("get cases config")
            return jsonify(json_200(cases))
        except json.JSONDecodeError as jse:
            current_app.logger.warning(jse)
            raise ConfProcessError(f"failed too read cases config: {jse}")

    if sys.platform != "win32":
        @api.route('/sp/new')
        def sp_new_interface():
            current_app.logger.info("sp interface built")
            p = ProcessHandle("/home/huangxiaojie/command/Sp_command_Mar8",env=sp_env,isshell=True)
            p.start()
            sp_process.append(sp_command_interface(p))

            return jsonify(json_200({"msg":"new interface has been created"}))

        @api.route("/sp/start",methods=gateway_methods["startexercise"])
        def sp_start_exercise():
            data = request.get_json()
            current_app.logger.debug(f'sp_start recieve {request.json}')
            # argdata = request.args.to_dict()
            # scenes = request.args.get("scenes","No",type=str)
            # ifstore = request.args.get("ifstore",-1,type=int)
            scenes:str = safewarp(data,"scenes","No")
            ifstore:int = safewarp(data,"ifstore",-1)
            assert_sp_(sp_process).unwrap()
            s = sp_process[0]
            jobs = {
                "anti":[s.start_exercise_nostore_antiterror,s.start_exercise_store_antiterror],
                "field":[s.start_exercise_nostore_field,s.start_exercise_store_field],
            }
            if scenes in ['anti','field'] and ifstore in [0,1]:
                jobs[scenes][int(ifstore)]().unwrap()
            else:
                raise SpinterfaceError("{} with store state {} not supported".format(scenes,ifstore))
            
            return jsonify(json_200({"msg":f"{scenes} with ifstore: {ifstore} started"}))

        @api.route("/sp/stop",methods=gateway_methods["stopexercise"])
        def sp_stop_exercise():
            current_app.logger.debug("sp_stop_exercise called to stop exercise")
            assert_sp_(sp_process).unwrap()
            sp_process[0].stop_exercises()
            return jsonify(json_200({"msg":"current process stoped"}))

        @api.route("/sp/list")
        def sp_list_store():
            assert_sp_(sp_process).unwrap()
            sp_process[0].list_store().unwrap()
            return jsonify({"store":sp_process[0].list_store().unwrap()})
        
        @api.route("/sp/replay",methods=gateway_methods["replay"])
        def sp_replay():
            current_app.logger.debug(f'{request.json} use in sp_replay')
            data = request.get_json()
            assert_sp_(sp_process).unwrap()
            id = safewarp(data,"saveid",-1)
            sp_process[0].replay_exercise(id)
            return jsonify({"msg":f"start replay using data {id}"})
        
        @api.route("/sp/destory")
        def destory():
            assert_sp_(sp_process).unwrap()
            if sp_process[0].started_lock or sp_process[0].select_lock:
                raise SpinterfaceError("Stop current exercise or finish selecting before exit")
            sp_process[0].sp_handler.stop()
            sp_process.clear()
            return jsonify(json_200({"msg":"sp command interface exit"}))
            
    def target_check():
        health_report = {}
        for each in commands.keys():
            if isinstance(commands[each],dict):
            # if each in uncheck:
            #     health_report[each] = "skip check"
            #     continue
                fpath = commands[each]["commands"].get("cmd","")
                if os.path.exists(fpath):
                    health_report[each] = {"exists": True}
                    health_report[each]["implable"] = os.access(fpath,os.X_OK)
                else:
                    health_report[each] = {"exists": False}
                    continue
        return health_report

    def start_handler(data:dict):
        (process_id, process_handle) = _start_process(data,commands).unwrap()
        process[str(process_id)] = process_handle
        return jsonify(json_200({
                        "process_id":process_id
                    }))

    def stop_handler(data:dict):
        current_app.logger.debug(f'use {data} in stop_process')
        return jsonify(json_200({
                "message":f"send SIGTERM to {_stop_process(data,process).unwrap()} "
            }))
    
    def _start_process(data:dict,commands:dict) -> Result:
        '''
        data struct:{"process":<ProcessHandle>,"command":<str>"(startcommand)}
        commands a commands dict load fom toml
        workdir a commands dict load fom toml
        excoms a commands dict load fom toml
        '''
        current_app.logger.debug(f'use {data} in start_process')
        token = safewarp(data,"token","")
        command = data.get("process_name","")
        # command = safewarp(data,"process_name","")
        process_id = uuid.uuid4()
        # process_isshell = boolwarp(safewarp(data,"process_isshell",""))
        # if process_isshell is None or command == "":
        #     raise PramasError(f"Consider check the parmas {'process_name' if command == '' else 'process_isshell'}")
        if command == "":
            raise PramasError(f"Consider check the parmas <process_name>")

        command_conf:dict = commands.get(command,{})
        if command_conf != {}:
            try:
                process_handle = {"process":ProcessHandle(args=command_conf.get("commands",{}).get("cmd","echo NaArgs"),\
                                                        cwd=command_conf.get("workdir"),\
                                                        isshell=command_conf.get("commands",{}).get('isshell',False),\
                                                        excom=command_conf.get('excom'),\
                                                        env=command_conf.get("env"))}
                process_handle["process"].start()
                process_handle["command"] = command
                current_app.logger.info(f"Process start with command {command} id: {process_id}")

                return Ok((process_id,process_handle))
            except Exception as e:
                return Err(ProcessRuntimeError(str(e)))
        else:
            return Err(ProcessNameError("Request command not found in conf.toml"))


    def _stop_process(data:dict,process:dict) -> Result:
        token = safewarp(data,'token',"")
        process_id = data.get("process_id","")

        if process_id not in process:
            raise ProcessIDError(f"{process_id} is not vaild id")
        
        try:
            process[process_id]["process"].stop()
            del process[process_id]
            return Ok(process_id)
        except Exception as e:
            return Err(ProcessRuntimeError(str(e)))
    
    def assert_sp_(process:list) -> Result:
        if len(process) == 0:
            return Err(SpinterfaceError("spinterface not created"))
        else:
            return Ok(1)

    return api
