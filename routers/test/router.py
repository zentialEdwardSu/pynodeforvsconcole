from flask import Blueprint,render_template

def Blue_print_constructer_test():
    test_api = Blueprint("testv1", __name__,template_folder="../../templates",url_prefix="/test")

    @test_api.route("/editor")
    def render_editor():
        return render_template('editor.html')
    @test_api.route("")
    def res_hello():
        return "Hello! This is a test area for script func"
    return test_api