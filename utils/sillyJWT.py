from hashlib import sha256
# from secret_key.secret_key import *
import hmac,base64,time

class EFJWTexpection(Exception):
    '''
    EFJWTERROR 是一个继承于 Exception 类的异常类,用于处理efficacx_jwt 模块遇到的异常
    '''
    def __init__(self,error):
        self.error_msg = error
    def __str__(self):
        return ("Efficacx Error occurred::{}".format(self.error_msg))

class Efficacx_jwt():
    '''
    自己造的轮子XD
        :在使用encode时要传入 user_id user_name secret 字段并等号表明 -> str
        :在使用verify时要传入 jwt 的对象 str 型 以及 secret(bytes型) -> bool
        :具体使用方式参照样例
        >>> jwt = Efficacx_jwt(user_id = "12345",user_name = "Edward",secret = secret)
        >>> verfiy = Efficacx_jwt(jwt = jwt_encode:str,secret = secret:bytes)'''
    def __init__(self,**kwargs) -> None:
        # print(kwargs)
        self.header = {
            "alg":"HS256",
            "typ":"JWT"
        }
        self.secret = 0
        self.exists_jwt = 0
        self.user_id = 0
        self.user_name = 0
        if "jwt" in kwargs:
            self.exists_jwt = kwargs["jwt"]
        if "user_name" in kwargs:
            self.user_name = kwargs["user_name"]
        if "user_id" in kwargs:
            self.user_id = kwargs["user_id"]
        if "secret" in kwargs:
            self.secret = kwargs["secret"]
            
    
    def encode(self)->str:
        if self.secret == 0:
            raise EFJWTexpection("secret missing")
        if self.user_name == 0:
            raise EFJWTexpection("user_name missing")
        if self.user_id == 0:
            raise EFJWTexpection("user_id missing")
        payload = {
            "exp" : time.time()+3600,
            "user_id": self.user_id,
            "user_name": self.user_name
        }
        print(payload)
        hm = hmac.new(self.secret,(base64.urlsafe_b64encode(bytes(str(self.header),encoding = "utf8"))+b"."+base64.urlsafe_b64encode(bytes(str(payload),encoding='utf8'))),digestmod=sha256).hexdigest()
        self.exists_jwt = base64.urlsafe_b64encode(bytes(str(self.header),encoding = "utf8"))+b"."+base64.urlsafe_b64encode(bytes(str(payload),encoding='utf8')) + b'.'+bytes(hm,encoding='utf8')
        return self.exists_jwt.decode()

    def verify_token(self)->bool:
        if self.exists_jwt == 0:
            raise EFJWTexpection("Jwt missing")
        elif self.exists_jwt == "":
            return False
        if type(self.exists_jwt)!=str:
            raise EFJWTexpection("type of JWT obj must be string")
        if self.secret == 0:
            raise EFJWTexpection("secret missing")
        if type(self.secret)!=bytes:
            raise EFJWTexpection("type of secret must be bytes")
        # t = bytes.decode(self.exists_jwt)
        t = self.exists_jwt
        ver_temp = t.split(".")

        to_ver = str.encode(ver_temp[0])+b"."+str.encode(ver_temp[1])
        # print(to_ver)
        payload = eval(base64.urlsafe_b64decode(ver_temp[1]))
        print(payload,type(payload))
        to_ver = hmac.new(self.secret,to_ver,digestmod=sha256).hexdigest()
        # print(to_ver)
        if to_ver == ver_temp[2]:
            if time.time() >= payload["exp"]:
                return False
                # raise EFJWTexpection("invaild token: token overdue")
            return True
        else:
            # raise EFJWTexpection("invaild token: token do not match")
            return False




#示例
# secret = b"abfvbfabyibyivbyia"    
# jwt = Efficacx_jwt(user_id = "12345",user_name = "Edward",secret = secret)            
# jwt_encode = jwt.encode()
# print(jwt_encode)
# try:
#     verfiy = Efficacx_jwt(jwt = jwt_encode,secret = secret)
#     time.sleep(6)#测试过期能否生效
#     verfiy.verify_token()
#     print("Success")
# except Exception as e:
#     print(e)