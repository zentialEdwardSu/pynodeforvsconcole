def safewarp(to_waarp,key,default):
    if key in to_waarp:
        return to_waarp[key]
    else:
        return default

def boolwarp(to_warp,default=None):
    if isinstance(to_warp,str):
        if to_warp == "True" or to_warp == "true":
            return True
        elif to_warp == "False" or to_warp == "false":
            return False
        else:
            return default
    elif isinstance(to_warp,int):
        if to_warp == 1:
            return True
        elif to_warp == 0:
            return False
        else:
            return default
    elif isinstance(to_warp,bool):
        return to_warp
    else:
        return default

def easy_wrap(var,default):
    if var != "":
        return var
    else:
        return default