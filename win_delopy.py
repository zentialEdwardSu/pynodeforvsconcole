from tornado.httpserver import HTTPServer
from tornado.wsgi import WSGIContainer
from app import app
from tornado.ioloop import IOLoop

port = 12207
print(f"Tornado server running on port {port}\n plz don't shut it down")
s = HTTPServer(WSGIContainer(app))
s.bind(port, "0.0.0.0")# listen '0.0.0.0'
s.start(1) 
IOLoop.current().start()