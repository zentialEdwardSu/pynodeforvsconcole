from utils.Rusty import Result,Ok,Err
import unittest

class TestResult(unittest.TestCase):
    def test_Ok(self):
        obj = {'1':2,}

        res = Ok(obj)
        except_res = Result(Ok=obj)
        
        self.assertEqual(res,except_res)
    
    def test_Err(self):
        exception = Exception("TEST")

        err = Err(exception)
        except_err = Result(Err=exception)

        self.assertEqual(err,except_err)
    
    def test_err(self):
        err = Err("TEST")

        self.assertEqual("TEST",err.unwrap())

    def test_unwrap_1(self):
        obj = {'1':2}

        res = Ok(obj)

        self.assertEqual(res.unwrap(),obj)
    
    def test_unwrap_2(self):
        exception = Exception("TEST")

        self.assertRaises(Exception,Err(exception).unwrap,'')

    def test_unwrap_3(self):
        a = Result()

        self.assertRaises(Exception,a.unwrap,'')
    
    def test_is_Ok(self):
        obj = {'1':2}

        res = Ok(obj)

        self.assertTrue(res.is_Ok())
    

if __name__ == '__main__':
    unittest.main()